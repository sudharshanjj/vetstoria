package com.vetstoria.exam.config;

import com.vetstoria.exam.service.CountryDataFetcher;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Configuration
public class RetrofitConfig {

    OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

    @Bean
    public Retrofit.Builder retrofit(){
        return  new Retrofit.Builder()
                .baseUrl("https://restcountries.eu/rest/v2/")
                .addConverterFactory(GsonConverterFactory.create());



    }

    @Bean
    public CountryDataFetcher countryDataFetcher(){
        return retrofit().client(httpClient.build()).build().create(CountryDataFetcher.class);
    }
}
