package com.vetstoria.exam.dto;



import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Setter
@Getter
public class Currency implements Serializable {
    private String code;
    private String name;
    private String symbol;
}
