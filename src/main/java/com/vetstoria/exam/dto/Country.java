package com.vetstoria.exam.dto;


import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Setter
@Getter
public class Country {
    String name;
    Long population;
    List<Currency> currencies;
    List<String> timezones;
}
