package com.vetstoria.exam.service;

import com.vetstoria.exam.dto.Country;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

import java.util.List;

public interface CountryDataFetcher {

    @GET("all")
    public Call<List<Country>> getAll();

    @GET("alpha/{code}")
    public Call<Country> getCountryByCode(@Path("code") String code);

    @GET("region/{regionName}")
    public Call<List<Country>> getCountryByRegion(@Path("regionName") String regionName);

}
