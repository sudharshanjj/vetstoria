package com.vetstoria.exam.service.impl;

import com.vetstoria.exam.dto.Country;
import com.vetstoria.exam.service.CountryDataFetcher;
import com.vetstoria.exam.service.CountryService;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import retrofit2.Response;

import java.util.*;
import java.util.stream.Collectors;

@Getter
@Setter
@Service
public class CountryServiceImpl implements CountryService {

    @Autowired
    CountryDataFetcher countryDataFetcher;

    @SneakyThrows
    public List<String> getCurrencyOfMultiCountry(){
        Response<List<Country>> countryResponse = countryDataFetcher.getAll().execute();
        Set<String> symbols = new HashSet<>();
        List<String> duplicateSymbols = countryResponse.body().stream()
                .flatMap(c -> c.getCurrencies().stream())
                .filter(s -> !symbols.add(s.getSymbol()))
                .map(d -> d.getSymbol())
                .distinct()
                .collect(Collectors.toList());
        return duplicateSymbols;
    }

    @SneakyThrows
    public String getTimeDiff(String source,String target ){
        Response<Country> sourceCountry = countryDataFetcher.getCountryByCode(source).execute();
        if(sourceCountry.code() == 404)
            throw new Exception(source+" : not valid");

        Response<Country> targetCountry = countryDataFetcher.getCountryByCode(target).execute();
        if(targetCountry.code() == 404)
            throw new Exception(source+" : not valid");

        Long diffInMin = (getInMinutes(targetCountry.body().getTimezones().get(0)) -
                getInMinutes(sourceCountry.body().getTimezones().get(0)));

        return (diffInMin>0?"+":"-")+(String.format("%02d", Math.abs(diffInMin/60))+":"+String.format("%02d",Math.abs(diffInMin%60)));
    }

    @SneakyThrows
    public List<Country> getCountriesByRegionOrderByPopulation(String region){
        Response<List<Country>> countriesByRegion = countryDataFetcher.getCountryByRegion(region).execute();
        List<Country> countries =  countriesByRegion.body().stream()
                .sorted(Comparator.comparingLong(Country::getPopulation).reversed())
                .collect(Collectors.toList());


        return countries;
    }

    private Long getInMinutes(String val){
        String s[] = val.split("UTC(\\+|\\-)")[1].split(":");
        return ((Long.parseLong(s[0])*60)+(Long.parseLong(s[1]))) * (val.contains("+")?1:-1);
    }
}
