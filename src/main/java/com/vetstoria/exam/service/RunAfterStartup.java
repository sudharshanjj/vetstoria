package com.vetstoria.exam.service;

import com.vetstoria.exam.dto.Country;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class RunAfterStartup {

    @Autowired
    CountryService countryService;

    @EventListener(ApplicationReadyEvent.class)
    public void runAfterStartup() {

        //1
        for (String s: countryService.getCurrencyOfMultiCountry())
            System.out.println(s);
        System.out.println("-------------------------------------------------");
        //2
        System.out.println(countryService.getTimeDiff("BI","LAO"));
        System.out.println(countryService.getTimeDiff("ATA","LKA"));
        try{
            System.out.println(countryService.getTimeDiff("BIXX","LAO"));
        }catch(Exception ex) {
            ex.printStackTrace();
        }
        System.out.println("-------------------------------------------------");
        //3
        for (Country s: countryService.getCountriesByRegionOrderByPopulation("europe"))
            System.out.printf("%,d|%s %n", s.getPopulation(),s.getName());
    }
}
