package com.vetstoria.exam.service;

import com.vetstoria.exam.dto.Country;

import java.util.List;

public interface CountryService {
    public List<String> getCurrencyOfMultiCountry();
    public String getTimeDiff(String source,String target);
    public List<Country> getCountriesByRegionOrderByPopulation(String region);
}
